```bash
docker container run --rm \
  --volume $(pwd):/data \
  registry.gitlab.com/daverona/docker/tiler \
    tile "image.svs"
```

```
Making tile images
Making thumbnail image
Retrieving header info
```

```
image_files
image.dzi
image.dzi.json
image.thumbnail.jpeg
image.meta.json
```
